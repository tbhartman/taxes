#!/usr/bin/env python3
"""what is it; introspective expressions

Support to save operations so I know what I did.   :py:class:`Manna`
simply wraps whatever you give it as an expression.  It accepts no
arguments as well.

>>> Manna()
<Manna object at 0x...>
>>> a = Manna(1)
>>> a.evaluate()
1
>>> Manna(1.1).evaluate()
1.1
>>> Manna([1,2,3]).evaluate()
[1, 2, 3]

Once you have a :py:class:`Manna` object, you can use normal syntax to
build expressions.

>>> c = a + 1
>>> c
<Add object at 0x...>
>>> c.evaluate()
2
>>> (2 - a).evaluate()
1

Remember, these are objects...

>>> a = [1]
>>> b = [2]
>>> c = Manna(a) + b
>>> c.evaluate()
[1, 2]
>>> a[0] = 2
>>> c.evaluate()
[2, 2]
>>> c.lock()
>>> a[0] = 3
>>> c.evaluate()
[2, 2]
>>> c.unlock()
>>> c.evaluate()
[3, 2]

And the whole point of this...see what is being done.

>>> MannaReport.ascii(c)
'[3] + [2]'
>>> MannaReport.ascii(Manna())
''
>>> c.lock()
>>> a[0] = 4
>>> MannaReport.ascii(c)
'[3] + [2]'
>>> c.unlock()
>>> MannaReport.ascii(c)
'[4] + [2]'

Watch out for order of operations...the actual parsing is handled in
normal Python fashion.

>>> a = (2 + 3 + Manna(5))
>>> a.evaluate()
10
>>> MannaReport.ascii(a)
'5 + 5'
>>> a = (Manna(5) + 2 * 3)
>>> a.evaluate()
11
>>> MannaReport.ascii(a)
'5 + 6'
>>> a = (5 + Manna(2) * 3)
>>> a.evaluate()
11
>>> MannaReport.ascii(a)
'5 + 2 * 3'

I try to handle custom Manna classes

>>> class TestManna(Manna):
...     _num_args = (4,)
>>> a = TestManna(1,2,3,4)
>>> MannaReport.ascii(a)
'TestManna(1, 2, 3, 4)'

OK...going to do something tricky...

>>> a = None + Manna(1)
>>> MannaReport.ascii(a)
'1'
>>> a = Manna(1) + None
>>> MannaReport.ascii(a)
'1'


"""
import operator
import copy



class Manna(object):
    #: number of arguments supported, or None for any amount
    _num_args = (0,1)
    def __init__(self, *operands):
        if self._num_args and len(operands) not in self._num_args:
            raise TypeError("Invalid number of arguments.")
        self._operands = operands
        self._locked = False
        self._value = None
        self._locked_operands = None

    def get_operands(self):
        if self.is_locked():
            return self._locked_operands
        else:
            return self._operands

    def lock(self):
        self._value = self.evaluate()
        self._locked_operands = copy.deepcopy(self.get_operands())
        self._locked = True

    def unlock(self):
        self._locked = False
        self._value = None
        self._locked_operands = None

    def is_locked(self):
        return self._locked

    def _func(self, *operand):
        return (operand or [None])[0]

    def operate(self, *operands):
        return self._func(*operands)

    def evaluate(self):
        if self.is_locked():
            return self._value
        else:
            operands = []
            for o in self.get_operands():
                if isinstance(o,Manna):
                    o = o.evaluate()
                operands.append(o)
            return self.operate(*operands)

    def __repr__(self):
        fmt = '<{} object at 0x{:0>16x}>'
        return fmt.format(self.__class__.__name__, id(self))
    # arithemtic
    def __add__(self, other):
        return Add(self, other)
    def __sub__(self, other):
        return Sub(self, other)
    def __mul__(self, other):
        return Mul(self, other)
    # def __matmul__(self, other):
    # def __truediv__(self, other):
    # def __floordiv__(self, other):
    # def __mod__(self, other):
    # def __divmod__(self, other):
    # def __pow__(self, other[, modulo]):
    # def __lshift__(self, other):
    # def __rshift__(self, other):
    # def __and__(self, other):
    # def __xor__(self, other):
    # def __or__(self, other):
    # # reverse arithmetic
    def __radd__(self, other):
        return Add(other, self)
    def __rsub__(self, other):
        return Sub(other, self)
    def __rmul__(self, other):
        return Mul(other, self)
    # def __rmatmul__(self, other):
    # def __rtruediv__(self, other):
    # def __rfloordiv__(self, other):
    # def __rmod__(self, other):
    # def __rdivmod__(self, other):
    # def __rpow__(self, other):
    # def __rlshift__(self, other):
    # def __rrshift__(self, other):
    # def __rand__(self, other):
    # def __rxor__(self, other):
    # def __ror__(self, other):
    # # inflex
    # def __iadd__(self, other):
    # def __isub__(self, other):
    # def __imul__(self, other):
    # def __imatmul__(self, other):
    # def __itruediv__(self, other):
    # def __ifloordiv__(self, other):
    # def __imod__(self, other):
    # def __ipow__(self, other[, modulo]):
    # def __ilshift__(self, other):
    # def __irshift__(self, other):
    # def __iand__(self, other):
    # def __ixor__(self, other):
    # def __ior__(self, other):
    # # unary operators
    # def __neg__(self):
    # def __pos__(self):
    # def __abs__(self):
    # def __invert__(self):
    # # conversion
    # def __complex__(self):
    # def __int__(self):
    # def __float__(self):
    def __round__(self, n=0):
        return Round(self, n)
    # def __index__(self):


class Func(Manna):
    _num_args = tuple()
    def _func(self, *args):
        return args[0](*args[1:])


class Unary(Manna):
    _num_args = (1,)


class Neg(Unary):
    _func = lambda i: -i

class Binary(Manna):
    _num_args = (2,)

class Round(Manna):
    _num_args = (1,2)
    _func = round

class Add(Binary):
    def __new__(cls, *operands):
        left, right = operands
        if left is None or right is None:
            only = left or right
            if isinstance(only, Manna):
                return only
            else:
                return Manna(only)
        return Binary.__new__(cls)

    _func = operator.add
class Sub(Binary):
    _func = operator.sub
class Mul(Binary):
    _func = operator.mul

#
# class Null(Unary):
#     def evaluate(self):
#         return self._get_value(self._operands[0])
#     def report(self):
#         op = self._get_items_for_report()
#         return '{}'.format(*op)
#
# class Round(Unary):
#     _symbol = 'round'
#     def __init__(self, operand, n=0):
#         super().__init__(operand)
#         self._n = n
#     def evaluate(self):
#         return round(self._get_value(self._operands[0]), self._n)
#

def display_ascii_func(l):
    l = list(l)
    func = l[0].split()[1]
    args = l[1:]
    return '{}({})'.format(func,', '.join(args))

class MannaReport(object):
    _FORMATS = {
            'ascii':{
                Manna: lambda l: (list(l) or [''])[0],
                Add:   lambda l: ' + '.join(l),
                Sub:   lambda l: ' - '.join(l),
                Mul:   lambda l: ' * '.join(l),
                Func:  display_ascii_func,
                },
            }

    @classmethod
    def set_formatter(cls, format_class, format, formatter):
        cls._FORMATS[format][format_class] = formatter

    @classmethod
    def ascii(cls, obj, newline=False):
        # build up a list
        ret = ''
        mannatype = isinstance(obj, Manna) and type(obj)
        if mannatype:
            opr = obj.get_operands()
            m = map(cls.ascii, opr)
            formatter = cls._FORMATS['ascii'].get(mannatype)
            if formatter is None:
                name = mannatype.__name__
                args = ', '.join(m)
                ret += '{}({})'.format(mannatype.__name__, args)
            else:
                ret += formatter(m)
        else:
            ret += repr(obj)
        return ret

