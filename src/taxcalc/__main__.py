#!/usr/bin/env python3
import sys
import pathlib
import argparse
import importlib

import logging

logger = logging.getLogger(__name__)

from ._version import get_versions


def test(args):
    sys.path.append(str(pathlib.Path(__file__).parent.parent))
    from . import tests
    sys.path.pop()
    return tests.test(args.verbose)

def calc(args):
    path = pathlib.Path(args.INPUT)
    if not path.is_file():
        logger.error('{!s} does not exist'.format(path))

    # load user script
    loader = importlib.find_loader(path.stem, [str(path.absolute().parent)])




def main():
    parser = argparse.ArgumentParser('taxcalc')
    parser.add_argument('-v','--verbose', action='store_true')
    parser.add_argument('-V','--version', action='store_true')

    if '-V' in sys.argv or '--version' in sys.argv:
        v = get_versions()
        print(v['version'])
        return 0

    b = parser.add_subparsers(dest='cmd')
    b.required = False
    tester = b.add_parser('test')
    tester.set_defaults(func=test)
    calcarg = b.add_parser('calc')
    calcarg.set_defaults(func=calc)
    calcarg.add_argument('INPUT')

    args = parser.parse_args()

    if args.verbose:
        level = logging.INFO
    else:
        level = logging.WARNING
    logging.basicConfig(level=level)

    return args.func(args)

if __name__ == '__main__':
    sys.exit(main())
