#!/usr/bin/env python3
# coding: utf-8
import collections
import time
import sys
import copy
import enum
import logging
import datetime
import math
import functools
import decimal
import pathlib
import io

#import jinja2
import pathlib
import importlib.machinery
_thispath = pathlib.Path(__file__).parent
_jinja_path = _thispath/'third_party'/'jinja2'/'__init__.py'

_jinja_loader = importlib.machinery.SourceFileLoader('jinja2',str(_jinja_path))
jinja2 = _jinja_loader.load_module()

from . import money
from . import manna

from ._version import get_versions
_version = get_versions()
del get_versions

logger = logging.getLogger(__name__)

#------------------------------------------------------------------------------#
#----------------------- generic classes and functions ------------------------#
#------------------------------------------------------------------------------#

class TaxError(Exception): pass
class TaxIdError(ValueError): pass

class FilingStatus(enum.Enum):
    SINGLE = 1
    JOINT = 2
    SEPARATE = 3
    HEAD = 4
    WIDOW = 5

class Person(object):
    def __init__(self, name, social, birthdate, citizen=True, status=None, spouse=None, dependents=None):
        self.name = name
        try:
            assert int(social)
            assert len(str(social)) == 9
        except (TypeError,AssertionError):
            logger.warning("Invalid social security number for {}".format(' '.join(name)))
        self.social = social

        #: list of dependents
        self.dependents = list(dependents or tuple())

        #: spouse
        self.spouse = None

        self.status = status

        self.dob = birthdate
        self.citizen = citizen

def USD(amount):
    """
    >>> USD(1)
    USD 1
    """
    return money.Money(amount, 'USD')

class TaxItem(object):
    """an object describing amounts

    >>> a = TaxItem('test description', USD(1.5))
    >>> a
    <TaxItem USD 1.50>
    >>> a.value + USD(1)
    USD 2.5

    """

    def __init__(self, description, value, document=(None,None)):
        self.description = description
        if not isinstance(value, money.Money):
            raise ValueError('value must be Money')
        self.value = value
        if isinstance(document, str):
            document = (document, True)
        if any(document) and pathlib.Path(document[0]).exists():
            self.document = (pathlib.Path(document[0]), document[1])
        else:
            self.document = (None, None)
    def __repr__(self):
        return '<{} {} {!s}>'.format(self.__class__.__name__,self.description,self.value)

class Deduction(TaxItem): pass
class Income(TaxItem): pass
class Giving(Deduction): pass
class Tax(Deduction): pass
class StateTax(Tax): pass
class RealEstateTax(Tax): pass
class PersonalPropertyTax(Tax): pass
class Interest(Income): pass
class MortageInterest(Deduction): pass
class Refund(Income): pass
class StateRefund(Income): pass
class FederalRefund(Income): pass

class IncompleteFormError(TaxError): pass

class LineId(tuple):
    """a form line identifier

    takes one of the following:
        * tuple
        * string
        * int
        * float
    and makes a tuple

    these must be hashable!

    >>> LineId(12)
    (12,)
    >>> LineId((12,'C'))
    (12, 'C')
    >>> LineId([1,2])
    (1, 2)
    >>> LineId([[],2])
    Traceback (most recent call last):
        ...
    TypeError: unhashable type: 'list'
    >>> str(LineId(1))
    '1'
    >>> str(LineId((1,'A')))
    '1A'
    >>> LineId('1')
    (1,)
    >>> LineId('1.1')
    (1.1,)
    >>> LineId('A')
    ('A',)

    """
    def __new__(cls, value):
        if isinstance(value, str):
            try:
                assert str(int(value)) == value
            except (ValueError, AssertionError) as e:
                try:
                    assert str(float(value)) == value
                except:
                    pass
                else:
                    value = float(value)
            else:
                value = int(value)
            value = (value,)
        elif isinstance(value, (int, float)):
            value = (value,)
        a = tuple.__new__(cls, value)
        hash(a)
        return a
    def __str__(self):
        return ''.join(map(str, self))
    def __hash__(self):
        return tuple.__hash__(self)
    def _rich_compare(self, other):
        order = [int,str]
        index = 0
        while True:
            if len(self) == index or len(other) == index:
                if len(self) == index and len(other) == index:
                    # I've gotten this far and both are out of range
                    # must be equal
                    return 0
                elif len(self) == index:
                    # self is out, but not other; other is greater
                    return -1
                else:
                    # other is out, but not self; self is greater
                    return 1
                break

            left = self[index]
            right = other[index]
            try:
                lefttype = order.index(type(left))
            except IndexError:
                lefttype = len(order)
            try:
                righttype = order.index(type(right))
            except IndexError:
                righttype = len(order)

            if not lefttype == righttype:
                left = lefttype
                right = righttype

            if left < right:
                return -1
            elif left > right:
                return 1
            elif left == right:
                index += 1
            else:
                raise ValueError('Cannot compare: {!r},{!r}'.format(self,other))

    # comparison
    def __lt__(self, other):
        return self._rich_compare(other) < 0
    def __le__(self, other):
        return self._rich_compare(other) <= 0
    def __eq__(self, other):
        return self._rich_compare(other) == 0
    def __gt__(self, other):
        return self._rich_compare(other) > 0
    def __ge__(self, other):
        return self._rich_compare(other) >= 0


class Line:
    _SETVALUE_COUNTER = 0
    """
    >>> Line(12, 'test', USD(1.1))
    <Line 12: USD 1.10>
    """
    def __init__(self, identifier, description, value=None, dedent=False, form=None):
        self.identifier = LineId(identifier)
        self.description = description
        self._dedent = dedent
        self._expression = None
        self.form = form

        self._mod_order = None
        self.reset()
        self.value = value
        self.hi = True

    @property
    def reason(self):
        if isinstance(self._expression, manna.Manna):
            return manna.MannaReport.ascii(self._expression)
        else:
            return None

    @property
    def value(self):
        if isinstance(self._expression, manna.Manna):
            return self._expression.evaluate()
        else:
            return self._expression
    @value.setter
    def value(self, value):
        self._mod_order = Line._SETVALUE_COUNTER
        Line._SETVALUE_COUNTER += 1
        self._expression = value

    @property
    def expression(self):
        if isinstance(self._expression, manna.Manna):
            return self._expression
        else:
            return None

    def __repr__(self):
        if not self.form:
            return '<{} {!s}: {!r}>'.format(self.__class__.__name__,self.identifier, self.value)
        else:
            return '<{} {},{!s}: {!r}>'.format(self.__class__.__name__,self.form.identifier,self.identifier, self.value)

    def reset(self):
        self.value = None

    def get_modified_order(self):
        return self._mod_order

    # arithemtic
    def __add__(self, other):
        return self.value + other
    def __sub__(self, other):
        return self.value - other
    # def __mul__(self, other):
    #     return self.value * other
    # def __matmul__(self, other):
    # def __truediv__(self, other):
    # def __floordiv__(self, other):
    # def __mod__(self, other):
    # def __divmod__(self, other):
    # def __pow__(self, other[, modulo]):
    # def __lshift__(self, other):
    # def __rshift__(self, other):
    # def __and__(self, other):
    # def __xor__(self, other):
    # def __or__(self, other):
    # # reverse arithmetic
    def __radd__(self, other):
        return self.__add__(other)
    def __rsub__(self, other):
        return (-self.value) + other
    # def __rmul__(self, other):
    #     return other * self.value
    # def __rmatmul__(self, other):
    # def __rtruediv__(self, other):
    # def __rfloordiv__(self, other):
    # def __rmod__(self, other):
    # def __rdivmod__(self, other):
    # def __rpow__(self, other):
    # def __rlshift__(self, other):
    # def __rrshift__(self, other):
    # def __rand__(self, other):
    # def __rxor__(self, other):
    # def __ror__(self, other):
    # # inflex
    # def __iadd__(self, other):
    # def __isub__(self, other):
    # def __imul__(self, other):
    # def __imatmul__(self, other):
    # def __itruediv__(self, other):
    # def __ifloordiv__(self, other):
    # def __imod__(self, other):
    # def __ipow__(self, other[, modulo]):
    # def __ilshift__(self, other):
    # def __irshift__(self, other):
    # def __iand__(self, other):
    # def __ixor__(self, other):
    # def __ior__(self, other):
    # # unary operators
    # def __neg__(self):
    # def __pos__(self):
    # def __abs__(self):
    # def __invert__(self):
    # # conversion
    # def __complex__(self):
    # def __int__(self):
    # def __float__(self):
    # def __round__(self[, n]):
    # def __index__(self):

class LineManna(manna.Unary):
    """
    >>> a = Line(1, 'test', USD(1))
    >>> b = LineManna(a)
    >>> manna.MannaReport.ascii(a)
    '<Line 1: USD 1>'
    >>> b.evaluate()
    USD 1
    """
    @staticmethod
    def _func(line):
        return line.value
manna.MannaReport.set_formatter(LineManna, 'ascii', lambda l: list(l)[0])


class SubLineManna(manna.Binary):
    """
    >>> a = Line(1, 'test', {'A':USD(1)})
    >>> b = LineManna(a)
    >>> manna.MannaReport.ascii(a)
    '<Line 1: USD 1>'
    >>> b.evaluate()
    USD 1
    """
    @classmethod
    def _func(cls, line, subitem):
        return line.value[subitem]
def _subline_fmt(args):
    line, subitem = list(args)
    idstr = str(line.id) + ':' + str(subitem)
    newline = Line(idstr, None, line[subitem])
    return newline
manna.MannaReport.set_formatter(
        SubLineManna,
        'ascii',
        _subline_fmt,
        )

class TaxDocument:
    #: form year
    year = None
    #: owner
    owner = None
    #: description
    description = None

    def __init__(self, owner, description, rounding):
        self.owner = owner
        self.description = description

        if callable(rounding):
            r = rounding
        elif rounding:
            def r(value):
                if value is None:
                    return value
                else:
                    try:
                        if value.evaluate() is not None:
                            return round(value)
                        else:
                            return value
                    except AttributeError:
                        return round(value)
        else:
            def r(value):
                return value
        #: rounding function
        self._rounding = r


class TaxReturn(TaxDocument):
    def __init__(self, year, description, data, starting_form_id, rounding=False):

        owner = [i for i in data if isinstance(i, Person)]
        if len(owner) == 0:
            raise TaxError('no Person found')
        elif len(owner) > 1:
            raise TaxError('multiple Persons found')
        owner = owner[0]

        super(TaxReturn, self).__init__(owner, description, rounding)
        self.year = year

        self.data = list(data)

        self.forms = []
        starting_form = get_form(year, starting_form_id)
        if not starting_form.year == year:
            raise ValueError('Starting form year mismatch {:d} != {:d}'.format(year, starting_form.year))
        starting_form = starting_form(owner, rounding=rounding, tax_return=self)
        self.forms.append(starting_form)

    def calculate(self):
        self.forms[0].calculate()


class Form(TaxDocument):
    """a tax form

    A tax form consists of personal information and a set of itemized lines.
    Personal information and a list of tax data are given to the constructor.
    The line items can then be accessed as items in the form.  Items are
    referenced either by a single number, if the line has an integer only label,
    or a tuple of (int, str).  For example,

    # >>> f = Form(None)
    # >>> f[12] is None
    # True
    # >>> f[12].value = 1.0
    # >>> f[12]
    # <Line 12: 1.0>
    # >>> f[(12,'C')] = USD(2.0)
    # >>> f[(12,'C')]
    # $2.00
    # >>> f.report(width=40)
    # ----------------------------------------
    #                   Form
    # ----------------------------------------
    # ______________________ [ 12]         1.0
    # ______________________ [12C]       $2.00
    #
    """

    #: form id this class provides
    identifier = None
    #: irs sequence number
    sequence = 0
    #: indicator if form is rounded
    rounding = False

    def __init__(self,
            owner,
            description=None,
            document=None,
            rounding=None,
            ):
        super(Form, self).__init__(owner, description, rounding)
        if document is None:
            if self.identifier is None:
                pass
            else:
                document = self.identifier.lower() + '.pdf'
        if isinstance(document, str):
            document = (document, True)
        if any(document) and pathlib.Path(document[0]).exists():
            self.document = (pathlib.Path(document[0]), document[1])
        else:
            self.document = (None, None)

        self._lines = {}
        for i in self._initialize_lines():
            self._lines[i.identifier] = i
            i.form = self

        self.reset()

    def _initialize_lines(self):
        return tuple()

    def reset(self):
        list(map(lambda i: i.reset(), self._lines.values()))

    def calculate(self):
        pass

    def __getitem__(self, key):
        try:
            return self._lines[LineId(key)]
        except KeyError:
            return None

    def __setitem__(self, key, value):
        try:
            self._lines[LineId(key)].value = value
        except KeyError:
            line = Line(LineId(key), None, value=value, form=self)
            self._lines[line.identifier] = line

    def get(self, *args, **kwargs):
        return self._lines.get(*args, **kwargs)

    def get_lines(self):
        return [self[k] for k in sorted(self._lines.keys())]

    def get_data(self, cls):
        return filter(lambda i: isinstance(i,cls), self._data)
    def sum_lines(self, lines_or_ids, roundfunc=None, multipliers=None):
        multipliers = multipliers or [None]*len(lines_or_ids)
        ret = []
        lines = []
        for i in lines_or_ids:
            if isinstance(i, Line):
                lines.append(i)
            else:
                lines.append(self[i])
        for l,m in zip(lines, multipliers):
            if l is None:
                continue
            if l.value is None:
                continue
            n = LineManna(l)
            if m is not None:
                n = manna.Mul(m,n)
            ret.append(n)
        if not ret:
            return None
        elif roundfunc:
            return sum(map(roundfunc,ret), None)
        else:
            return sum(ret, None)

class StaticForm(Form):
    sequence = -1
    def __init__(self,
            owner,
            description=None,
            data=None,
            document=None,
            ):
        super(StaticForm, self).__init__(owner, description, document, rounding=False)
        self._data = data
        for k,v in self._data.items():
            self[k].value = v

    def calculate(self):
        pass


class GenericStaticForm(StaticForm):
    def __init__(self,
            year,
            identifier,
            owner,
            description=None,
            document=None,
            ):
        super(GenericStaticForm, self).__init__(owner, description, document)
        self.year = year
        self.identifier=identifier


class CalculatedForm(Form):
    def __init__(self,
            owner,
            description=None,
            document=None,
            rounding=None,
            tax_return=None,
            ):
        super(CalculatedForm, self).__init__(owner, description, document, rounding)
        self._return = tax_return

class Worksheet(CalculatedForm):
    def __init__(self, *args, **kwargs):
        super(Worksheet, self).__init__(*args, **kwargs)
        self.description = type(self).description

def print_tax_returns(returns, output):
    import io
    import subprocess
    def esc(value):
        if not isinstance(value, str):
            value = repr(value)
        ret = []
        for i in value:
            ret.append({
                '<':r'\textless{}',
                '>':r'\textgreater{}',
                '_':r' ',
                }.get(i,i))
        return ''.join(ret)
    env = jinja2.Environment(
            loader = jinja2.FileSystemLoader(str(pathlib.Path(__file__).parent / '_templates')),
            autoescape = False,
            )
    env.filters.update({
        'esc': esc,
    })
    template = env.get_template('basic.tex')


    try:
        assert len(returns) > 0
    except AssertionError:
        raise ValueError('no returns given')
    try:
        assert len(set([i.owner for i in returns])) == 1
    except AssertionError:
        raise ValueError('owners must be the same for all returns')
    try:
        assert len(set([i.year for i in returns])) == 1
    except AssertionError:
        raise ValueError('year must be the same for all returns')

    variables = {
            'name': ' '.join(returns[0].owner.name),
            'year': returns[0].year,
            'version': _version['version'],
            'revision': _version['full-revisionid'],
            }
    if _version['dirty']:
        variables['revision'] = 'dirty'
    if returns[0].owner.spouse:
        variables['spouse'] = ' '.join(returns[0].owner.spouse.name)


    variables['returns'] = returns
    support_forms = set()
    for r in returns:
        r.forms.sort(key=lambda a: a.sequence)
        support_forms.update(filter(lambda a: isinstance(a,Form), r.data))
    variables['support'] = collections.OrderedDict()
    support_forms = sorted(support_forms, key=lambda i: (i.identifier, i.description))
    variables['support']['Forms'] = support_forms

    docs = filter(lambda i: isinstance(i,TaxItem), sum((r.data for r in returns), []))
    docs = list(docs)
    doctypes = sorted(type(i).__name__ for i in docs)
    for t in doctypes:
        variables['support'][t] = []
        for d in sorted(docs, key=lambda i: i.description):
            if type(d).__name__ == t and d not in variables['support'][t]:
                variables['support'][t].append(d)

    # old_stdout = sys.stdout
    # with io.StringIO() as f:
    #     sys.stdout = f
    #     from .tests import test
    #     test(verbose=True)
    #     sys.stdout = old_stdout
    #     f.seek(0)
    #     variables['taxcalc_test'] = f.read()

    output = pathlib.Path(output)
    output.write_text(template.render(**variables))

    # def line_key(item):
    #     return (item.get_modified_order() or 0, item.form.sequence, item.identifier)
    # lines = sorted(lines, key=line_key)
    # for line in lines:
    #     expr = line.expression
    #     if expr:
    #         print(manna.MannaReport.ascii(expr))
    #     try:
    #         print('{:s} {!s:10s}: {!s}'.format(line.form.identifier, line.identifier, line.value))
    #     except:
    #         import pdb; pdb.set_trace()
    # for i in forms:
    #     i.report(width)


_FORMS = {}

def get_form(year, name):
    return _FORMS[year,name]

def register_form(form_class):
    assert form_class.identifier is not None
    _FORMS[form_class.year, form_class.identifier] = form_class
    return form_class

#------------------------------------------------------------------------------#
#-------------------- 2016 specific classes and functions ---------------------#
#------------------------------------------------------------------------------#

def taxtable(income, status):
    # joint only
    D = decimal.Decimal
    if status is not FilingStatus.JOINT:
        raise NotImplementedError()
    if income < USD(18550):
        tax = income * D(0.1)
    elif income < USD(75300):
        tax = USD(1855) + D(0.15) * (income - USD(18550))
    elif income <= USD(151900):
        tax = USD(10367.50) + D(0.25) * (income - USD(75300))
    else:
        raise NotImplementedError()
    return round(tax,2)

@register_form
class W2(StaticForm):
    year = 2016
    identifier = 'W-2'
    sequence = -1
    def _initialize_lines(self):
        yield Line(1, 'Wages, tips, other comp.', USD)
        yield Line(2, 'Federal income tax withheld', USD)
        yield Line(3, 'Social security wages', USD)
        yield Line(4, 'Social security tax withheld', USD)
        yield Line(5, 'Medicare wages and tips', USD)
        yield Line(6, 'Medicare tax withheld', USD)
        yield Line(12, None, tuple)
        yield Line(14, 'Other')
        yield Line(15, "Employer's state ID", tuple)
        yield Line(16, 'State wages, tips, etc.', USD)
        yield Line(17, 'State income tax', USD)
        yield Line(18, 'Local wages, tips, etc.', USD)
        yield Line(19, 'Local income tax', USD)
        yield Line(20, 'Locality name', USD)

@register_form
class F1098(StaticForm):
    year = 2016
    identifier = 'F1098'
    sequence = -1
    def _initialize_lines(self):
        yield Line(1,"Mortgage interest received",USD)
        yield Line(2,"Outstanding mortgage",USD)
        yield Line(10,'Real estate taxes',USD)
@register_form
class F1099G(StaticForm):
    year = 2016
    identifier = 'F1099G'
    sequence = -1
    def _initialize_lines(self):
        yield Line(2,"State or local income tax refund",USD)
        yield Line(3,'Real estate taxes', int)

@register_form
class ScheduleB(CalculatedForm):
    year = 2016
    identifier = 'F1040SB'
    sequence = 8
    def _initialize_lines(self):
        yield Line(2,None, USD)
        yield Line(4,None, USD)
        yield Line(6,None, USD)

    def calculate(self):
        r = self._rounding

        interest = filter(lambda i: isinstance(i,Interest), self._return.data)
        self[2].value = list(interest)

        self[4].value = sum(map(r,map(LineManna,self[2].value)), None)

        if self[4].value > USD(1500):
            logger.warning('Schedule B Part III must be filled')

@register_form
class ScheduleA(CalculatedForm):
    year = 2016
    identifier = 'F1040SA'
    sequence = 7
    def _initialize_lines(self):
        yield Line(5,'State and local', USD)
        yield Line(6,'Real estate taxes', USD)
        yield Line(7,'Personal property taxes', USD)
        yield Line(9,'Taxes you paid', USD)
        yield Line(10, 'Home mortgage interest from Form 1098', USD)
        yield Line(15, 'Interest you paid', USD)
        yield Line(16, 'Gifts by cash or check', USD, dedent=True)
        yield Line(19, 'Charitable giving', USD)
        yield Line(29, 'Total itemized deductions', USD)

    def calculate(self):
        r = self._rounding
        w2 = list(
                filter(lambda i: isinstance(i, Form) and i.identifier == 'W-2',
                       self._return.data))
        f1098 = list(
                filter(lambda i: isinstance(i, Form) and i.identifier == 'F1098',
                       self._return.data))

        state_local_taxes = sum((LineManna(i[17]) for i in w2), None)
        for i in w2:
            if 'SDI' in i[14].value:
                state_local_taxes += SubLineManna(i[14], 'SDI')
        self[5].value = r(state_local_taxes)

        line_10 = [i[10] for i in f1098]

        self[6].value = self.sum_lines(line_10,r)
        proptax = filter(lambda i: isinstance(i,PersonalPropertyTax), self._return.data)
        self[7].value = r(sum(map(LineManna,proptax), None))
        self[9].value = self.sum_lines([5,6,7,8])

        line_1 = [i[1] for i in f1098]
        self[10].value = self.sum_lines(line_1,r)
        self[15].value = self.sum_lines([10,11,12,13,14])

        giving = filter(lambda i: isinstance(i,Giving), self._return.data)
        self[16].value = r(sum(map(LineManna,giving), None))
        self[19].value = self.sum_lines([16,17,18])

        self[29].value = self.sum_lines([4,9,15,19,20,27,28])

class ChildTaxCreditWorksheet(Worksheet):
    year = 2016
    identifier = 'ChildTax'
    description = 'Child Tax Credit Worksheet'
    def _initialize_lines(self):
        yield Line(1, None, USD)
        yield Line(2, None, USD)
        yield Line(3, None, USD)
        yield Line(4, None, USD)
        yield Line(5, None, USD)
        yield Line(6, None, USD)
        yield Line(7, None, USD)
        yield Line(8, None, USD)
        yield Line(9, None, USD)
        yield Line(10,None, USD)
    def calculate(self):
        r = self._rounding

        f1040 = list(
                filter(lambda i: i.identifier == 'F1040',
                       self._return.forms))[0]
        dependents = 0
        for d in self.owner.dependents:
            if d.dob > datetime.datetime(2017-17,1,1):
                if d.citizen:
                    dependents += 1
        self[1].value = manna.Manna(dependents) * USD(1000)
        self[2].value = LineManna(f1040[38])
        self[3].value = {FilingStatus.JOINT:   USD(110000),
                         FilingStatus.SEPARATE:USD(55000),
                         FilingStatus.HEAD:    USD(75000),
                         FilingStatus.SINGLE:  USD(75000),
                         FilingStatus.WIDOW:   USD(75000),
                        }[self.owner.status]

        if self[2].value > self[3].value:
            self[4].value = math.ceil((self[2] - self[3])/1000) * 1000
            self[5].value = r(LineManna(self[4]) * decimal.Decimal(0.05))
        else:
            self[4].value = None
            self[5].value = 0

        self[6].value = manna.Func(max, USD(0), LineManna(self[1]) - LineManna(self[5]))

        if f1040[47].value:
            self[7].value = r(LineManna(f1040[47]))
        else:
            self[7].value = USD(0)

        self[8].value = self.sum_lines(list(filter(None,[f1040[i] for i in [48,49,50,51]])))
        for formid,lineid in (
                ('F5695', 30), ('F8910', 15), ('F8936', 23), ('F1040SR', 22)):
            f = list(
                    filter(lambda i: i.identifier == formid,
                           self._return.forms))
            if not f:
                continue
            line = f[0].get(lineid)
            if line:
                self[8].value += LineManna(line)
        if self[8].value:
            self[8].value = r(self[8])
        else:
            self[8].value = USD(0)

        self[9].value = manna.Func(max, USD(0), LineManna(self[7]) - LineManna(self[8]))
        self[10].value = manna.Func(min, LineManna(self[6]), LineManna(self[9]))




@register_form
class F1040(CalculatedForm):
    year = 2016
    identifier = 'F1040'
    supports = ('SB',)
    def _initialize_lines(self):
        yield Line((6,'d'), 'Total excemptions', int)
        yield Line(7, 'Wages', USD)
        yield Line((8,'a'), 'Taxable interest', USD)
        yield Line((9,'a'), 'Ordinary dividends', USD)
        yield Line(10, 'Taxable refund', USD)
        yield Line(22, 'Total income', USD)
        yield Line(36, None, USD)
        yield Line(37, 'AGI', USD)
        yield Line(38, 'AGI', USD)
        yield Line(40, 'Standard or itemized deductions', USD)
        yield Line(41, None, USD)
        yield Line(42, 'Excemptions', USD)
        yield Line(43, None, USD)
        yield Line(44, None, USD)
        yield Line(47, None, USD)
        yield Line(49, None, USD)
        yield Line(52, 'Child tax credit', USD)
        yield Line(55, None, USD)
        yield Line(56, None, USD)
        yield Line(63, 'total tax', USD)
        yield Line(56, None, USD)
        yield Line(64, 'total payments', USD)
        yield Line((66,'a'), None, USD)
        yield Line(74, None, USD)
        yield Line(75, None, USD)
        yield Line(78, None, USD)

    @staticmethod
    def get_standard_deduction(status):
        ret = {FilingStatus.SINGLE:6300,
               FilingStatus.JOINT:12600,
               FilingStatus.SEPARATE:6300,
               FilingStatus.WIDOW:12600,
               FilingStatus.HEAD:9300,
               }[status]
        return USD(ret)

    def calculate(self):
        r = self._rounding
        self.reset()

        excemptions = 1
        if self.owner.spouse:
            excemptions += 1
        excemptions += len(self.owner.dependents)
        self[(6,'d')].value = excemptions

        w2s = list(
                filter(lambda i: isinstance(i, Form) and i.identifier == 'W-2',
                       self._return.data))
        self[7].value = r(sum((LineManna(i[1]) for i in w2s), None))

        sb = list(
                filter(lambda i: i.identifier == 'F1040SB',
                       self._return.forms))
        if not sb:
            sb = get_form(self.year, 'F1040SB')(self.owner, tax_return=self._return, rounding=self._rounding)
            self._return.forms.append(sb)
        sb.calculate()
        interest = r(LineManna(sb[4]))
        dividends = r(LineManna(sb[6]))
        if not (interest.evaluate() > USD(0) or dividends.evaluate() > USD(0)):
            self._return.forms.remove(sb)
        self[8,'a'].value = interest
        self[9,'a'].value = dividends

        # refunds
        refunds = list(
                filter(lambda i: isinstance(i,Form) and i.identifier == 'F1099G',
                       self._return.data))
        refunds = [i[2] for i in refunds]
        if refunds:
            self[10].value = r(sum(map(LineManna, refunds), None))
        else:
            self[10].value = None

        self[22].value = self.sum_lines([7,(8,'a'),(9,'a'),10,11,12,13,14,(15,'b'),(16,'b'),
             17,18,19,(20,'b'),21])

        self[36].value = self.sum_lines([23,24,25,26,27,28,29,30,(31,'a'),32,33,34,35])
        self[37].value = self.sum_lines([22,36],multipliers=[None,-1])
        self[38].value = self.sum_lines([37])

        sa = list(
                filter(lambda i: i.identifier == 'F1040SA',
                       self._return.forms))
        if not sa:
            sa = get_form(self.year, 'F1040SA')(self.owner, tax_return=self._return, rounding=self._rounding)
            self._return.forms.append(sa)
        sa.calculate()
        standard = self.get_standard_deduction(self.owner.status)
        if sa[29].value > standard:
            self[40].value = LineManna(sa[29])
        else:
            logging.warning('Using standard deduction because {} < {}'.format(sch_a[29], standard))
            self[40].value = standard
            self._return.forms.remove(sa)


        self[41].value = self.sum_lines([38,40],multipliers=[None,-1])
        if self[38].value <= USD(155650):
            self[42].value = LineManna(self[(6,'d')]) * USD(4050)
        else:
            raise NotImplementedError('See instructions on f1040 line 42')
        self[43].value = self.sum_lines([41,42], multipliers=[None,-1])
        self[44].value = r(manna.Func(taxtable, LineManna(self[43]), self.owner.status))

        self[47].value = self.sum_lines([44,45,46])

        # child tax credit
        wksh = ChildTaxCreditWorksheet(self.owner, tax_return=self._return, rounding=self._rounding)
        wksh.calculate()
        if wksh[10].value or USD(0) > USD(0):
            self[52].value = LineManna(wksh[10])
        self._return.data.append(wksh)

        self[55].value = self.sum_lines(list(range(48,55)))
        self[56].value = manna.Func(max, USD(0), LineManna(self[47]) - LineManna(self[55]))
        self[63].value = self.sum_lines([56,57,58,59,(60,'a'),(60,'b'),61,62])

        self[64].value = self.sum_lines([i[2] for i in w2s], r)
        self[74].value = self.sum_lines([64,65,(66,'a'),67,68,69,70,71,72,73])

        self[75].value = manna.Func(max, USD(0), LineManna(self[74]) - LineManna(self[63]))

        self[78].value = manna.Func(max, USD(0), LineManna(self[63]) - LineManna(self[74]))

class RiTaxWorksheet(Worksheet):
    year = 2016
    identifier = 'RiTax'
    description = 'Rhode Island tax calculation worksheet'
    def _initialize_lines(self):
        yield Line('a', 'RI-1040 Line 7', USD)
        yield Line('b', 'Multiplication amount', USD)
        yield Line('c', 'a * b', USD)
        yield Line('d', 'Subtract amount', USD)
        yield Line('TAX', 'c - d', USD)
    def calculate(self):
        r = self._rounding

        f1040 = list(
                filter(lambda i: i.identifier == 'RI_F1040',
                       self._return.forms))[0]
        line7 = f1040[7]
        if line7.value < USD('60850'):
            self['b'].value = decimal.Decimal('0.0375')
            self['d'].value = USD('0')
        elif line7.value < USD('138300'):
            self['b'].value = decimal.Decimal('0.0475')
            self['d'].value = USD('608.50')
        else:
            self['b'].value = decimal.Decimal('0.0599')
            self['d'].value = USD('2323.42')
        self['a'].value = LineManna(line7)
        self['c'].value = r(LineManna(line7) * LineManna(self['b']))
        self['TAX'].value = LineManna(self['c']) - LineManna(self['d'])

@register_form
class RiScheduleW(CalculatedForm):
    year = 2016
    identifier = 'RI_SchW'
    sequence = 8
    def _initialize_lines(self):
        yield Line(16,'RI Tax withheld', USD)
        yield Line(17,'Count of RI Tax withheld', USD)

    def calculate(self):
        r = self._rounding
        w2 = list(
                filter(lambda i: isinstance(i,Form) and i.identifier == 'W-2',
                       self._return.data))
        withheld = [i[17] for i in w2]
        self[16].value = r(sum(map(LineManna,withheld), None))
        self[17].value = len(withheld)


@register_form
class RiScheduleU(CalculatedForm):
    year = 2016
    identifier = 'RI_SchU'
    sequence = 7
    def _initialize_lines(self):
        yield Line(5,'Federal AGI', USD)
        yield Line(6,'Use tax due', USD)
        yield Line(8,'Use tax due', USD)

    def calculate(self):
        r = self._rounding
        f1040 = list(
                filter(lambda i: i.identifier == 'RI_F1040',
                       self._return.forms))[0]
        line1 = f1040[1]

        self[5].value = LineManna(line1)
        table = ((USD( '6329'),USD( '5')),
                 (USD('12657'),USD('10')),
                 (USD('18986'),USD('15')),
                 (USD('25315'),USD('20')),
                 (USD('31644'),USD('25')),
                 (USD('37972'),USD('30')),
                 (USD('44301'),USD('35')),
                 (USD('50630'),USD('40')),
                 (USD('56958'),USD('45')),
                 (USD('63287'),USD('50')),
                 (USD('69616'),USD('55')),
                 (USD('75945'),USD('65')),
                 )
        for agi,tax in table:
            if line1.value < agi:
                self[6].value = tax
                break
        if line1.value > USD('75944'):
            self[6].value = r(LineManna(line1) * decimal.Decimal('0.0008'))
        self[8].value = LineManna(self[6])

@register_form
class RI_F1040(CalculatedForm):
    year = 2016
    identifier = 'RI_F1040'
    supports = ('RI_SW',)
    def _initialize_lines(self):
        yield Line(1, 'Federal AGI', USD)
        yield Line(2, 'Net modifications to Federal AGI', USD)
        yield Line(3, 'Modified Federal AGI', USD)
        yield Line(4, 'Deductions', USD)
        yield Line(5, None, USD)
        yield Line(6, 'Exceptions', USD)
        yield Line(7, 'RI Taxable Income', USD)
        yield Line(8, 'RI income tax', USD)
        yield Line((9,'a'), 'RI percentage of allowable Federal credit', USD)
        yield Line((9,'b'), 'RI Credit for income taxes paid to other states', USD)
        yield Line((9,'c'), 'Other RI credits', USD)
        yield Line((9,'d'), 'Total RI credits', USD)
        yield Line((10,'a'), 'RI income tax after credits', USD)
        yield Line((10,'b'), 'Recapture of prior year credits', USD)
        yield Line(11, 'RI checkoff contributions', USD)
        yield Line(12, 'USE/SALES tax due', USD)
        yield Line(13, 'Total RI tax', USD)
        yield Line((14,'a'), 'RI 2016 income tax withheld', USD)
        yield Line((14,'b'), '2016 estimated tax payments', USD)
        yield Line((14,'c'), 'Property tax relief credit', USD)
        yield Line((14,'d'), 'RI earned income credit', USD)
        yield Line((14,'e'), 'RI Residential Lead Paint Credit', USD)
        yield Line((14,'f'), 'Other payments', USD)
        yield Line((14,'g'), 'Total Payments and Credits', USD)
        yield Line((15,'a'), 'Amount due', USD)
        yield Line(16, 'Amount overpaid', USD)
        yield Line(17, 'Amount overpaid to be refunded', USD)
        yield Line(19, None, USD)
        yield Line(20, None, USD)
        yield Line(21, None, USD)
        yield Line(22, None, USD)
        yield Line(38, None, USD)
        yield Line(39, None, USD)
        yield Line(40, None, USD)

    @staticmethod
    def get_standard_deduction(status):
        ret = {FilingStatus.SINGLE:8300,
               FilingStatus.JOINT:16600,
               FilingStatus.SEPARATE:8300,
               FilingStatus.WIDOW:16600,
               FilingStatus.HEAD:12450,
               }[status]
        return USD(ret)

    def calculate(self):
        r = self._rounding
        self.reset()

        f1040 = []
        for i in self._return.data:
            if isinstance(i, TaxReturn):
                for j in i.forms:
                    if j.identifier == 'F1040':
                        f1040.append(j)
        assert len(f1040) == 1
        fed1040 = f1040[0]
        w2s = list(
                filter(lambda i: isinstance(i, Form) and i.identifier == 'W-2',
                       self._return.data))

        self[1].value = LineManna(fed1040[37])
        self[2].value = USD(0)
        self[3].value = self.sum_lines([1,2])
        self[4].value = self.get_standard_deduction(self.owner.status)
        self[5].value = LineManna(self[3]) - LineManna(self[4])
        self[6].value = LineManna(fed1040[6,'d']) * USD('3900')
        if self[3].value > USD('193000'):
            logger.warning('see Excemption worksheet')

        self[7].value = LineManna(self[5]) - LineManna(self[6])
        self[8].value = USD('4000')

        wksh = RiTaxWorksheet(self.owner, tax_return=self._return, rounding=self._rounding)
        wksh.calculate()
        self._return.data.append(wksh)
        self[8].value = LineManna(wksh['TAX'])

        self[19].value = LineManna(self[8])
        self[20].value = LineManna(fed1040[49])
        if self[20].value is not None:
            self[21].value = r(LineManna(self[20]) * 0.25)
            if self[21].evaluate() > self[19].evaluate():
                self[22].value = LineManna(self[19])
            else:
                self[22].value = LineManna(self[21])
        else:
            self[22].value = LineManna(self[20])
        self[9,'a'].value = LineManna(self[22])

        self[38].value = LineManna(fed1040[66,'a'])
        self[39].value = 0.125
        if self[38].value is not None:
            self[40].value = LineManna(self[38]) * LineManna(self[39])
            self[14,'d'].value = LineManna(self[22])

        self[9,'d'].value = self.sum_lines([(9,'a'),(9,'b'),(9,'c')])
        if not self[9,'d'].value:
            self[9,'d'].value = USD('0')
        self[10,'a'].value = LineManna(self[8]) - LineManna(self[9,'d'])

        use = get_form(self.year, 'RI_SchU')(self.owner, tax_return=self._return, rounding=self._rounding)
        self._return.forms.append(use)
        use.calculate()
        self[12].value = LineManna(use[8])

        self[13] = self.sum_lines([(10,'a'),(10,'b'), 11, 12])


        sw = get_form(self.year, 'RI_SchW')(self.owner, tax_return=self._return, rounding=self._rounding)
        self._return.forms.append(sw)
        sw.calculate()
        self[14,'a'].value = LineManna(sw[16])
        self[14,'g'].value = LineManna(self[14,'a'])

        if self[13].value > self[14,'g'].value:
            self[15,'a'].value = LineManna(self[13]) - LineManna(self[14,'g'])
        else:
            self[16].value = LineManna(self[14,'g']) - LineManna(self[13])
            self[17].value = LineManna(self[16])

